import axios from 'axios'

export const uploadFile = (data, url = '/api/jobFileDatasource/upload') => {
  return new Promise((resolve, reject) => {
    axios.post(url, data, { headers: { 'Content-Type': 'multipart/form-data' }}).then((res) => {
      resolve(res)
    }).catch((err) => {
      reject(err)
    })
  })
}
